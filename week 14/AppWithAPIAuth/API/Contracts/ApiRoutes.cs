﻿namespace API.Contracts
{
    // The idea is good, but fuck this for now.
    /// <summary>
    /// A contract for the routes the Api is going to be using.
    /// </summary>
    public static class ApiRoutes
    {
        public const string Root = "api";

        /// <summary>
        /// This is NOT a RESTful way of doing it.
        /// But that is okay for this scenario. Reason being that this should actually be an entirely different project.
        /// The identity service should be a service that is not part of the API.
        /// But we are going to keep it here for simplicity.
        /// </summary>
        public static class Identity
        {
            public const string Login = Root + "/Login";
            public const string Register = Root + "/Register";
            public const string Refresh = Root + "/Refresh";
            public const string UserInfo = Root + "/Userinfo";
        }
    }
}
