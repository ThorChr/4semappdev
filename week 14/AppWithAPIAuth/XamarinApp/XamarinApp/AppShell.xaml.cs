﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using XamarinApp.ViewModels;
using XamarinApp.Views;
using XamarinApp.Views.SettingsPages;

namespace XamarinApp
{
    public partial class AppShell : Xamarin.Forms.Shell
    {
        public AppShell()
        {
            InitializeComponent();
            // I have to register routes, whenever they ARE NOT specified in the AppShell.xaml file.
            Routing.RegisterRoute(nameof(RegisterPage), typeof(RegisterPage));
            Routing.RegisterRoute(nameof(LoginPage), typeof(LoginPage));
            
            // Routes for settings pages.
            Routing.RegisterRoute(nameof(ThemeSettingsPage), typeof(ThemeSettingsPage));
            Routing.RegisterRoute(nameof(AuthenticationSettingsPage), typeof(AuthenticationSettingsPage));
        }

        private async void OnMenuItemClicked(object sender, EventArgs e)
        {
            await Shell.Current.GoToAsync("//LoginPage");
        }
    }
}
