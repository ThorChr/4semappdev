﻿using System;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using XamarinApp.Helpers;
using XamarinApp.Services;

namespace XamarinApp
{
    public partial class App : Application
    {

        public App()
        {
            InitializeComponent();

            // Theme handling - Sets the theme to whatever the preferences says it should be.
            ThemeHandler.SetTheme();

            // Register services.
            DependencyService.Register<IAuthenticationService, AuthenticationService>();

            MainPage = new AppShell();
        }

        protected override void OnStart()
        {
            OnResume();
        }

        protected override void OnSleep()
        {
            ThemeHandler.SetTheme();
            RequestedThemeChanged -= App_RequestedThemeChanged;
        }

        protected override void OnResume()
        {
            ThemeHandler.SetTheme();
            RequestedThemeChanged += App_RequestedThemeChanged;
        }


        private void App_RequestedThemeChanged(object sender, AppThemeChangedEventArgs e)
        {
            MainThread.BeginInvokeOnMainThread(() => 
            {
                ThemeHandler.SetTheme();
            });
        }
    }
}
