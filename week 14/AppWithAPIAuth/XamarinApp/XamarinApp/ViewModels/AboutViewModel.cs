﻿using MvvmHelpers;
using System;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;
using XamarinApp.Models;

namespace XamarinApp.ViewModels
{
    public class AboutViewModel : ViewModelBase
    {
        public ObservableRangeCollection<TestItem> Collection { get; set; }
        
        public string Token { get; set; }
        public AboutViewModel()
        {
            Title = "About";
            OpenWebCommand = new Command(async () => await Browser.OpenAsync("https://aka.ms/xamarin-quickstart"));
            Token = Preferences.Get("Token", "Failed to get token. Please logout and try again.");

            Collection = new ObservableRangeCollection<TestItem>()
            {
                new TestItem { Title = "Item 1", Description = "This is an item description." },
                new TestItem { Title = "Item 2", Description = "This is an item description." },
                new TestItem { Title = "Item 3", Description = "This is an item description." },
                new TestItem { Title = "Item 4", Description = "This is an item description." },
                new TestItem { Title = "Item 5", Description = "This is an item description." }
            };
        }

        public ICommand OpenWebCommand { get; }
    }
}