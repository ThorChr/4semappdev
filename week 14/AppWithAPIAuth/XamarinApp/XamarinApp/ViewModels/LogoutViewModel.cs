﻿using MvvmHelpers.Commands;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using XamarinApp.Services;
using XamarinApp.Views;

namespace XamarinApp.ViewModels
{
    public class LogoutViewModel : ViewModelBase
    {
        private readonly IAuthenticationService _authService;
        public AsyncCommand LogoutCommand { get; set; }

        public LogoutViewModel()
        {        
            LogoutCommand = new AsyncCommand(Logout);
            _authService = DependencyService.Get<IAuthenticationService>();

            Logout();
        }
        
        private async Task Logout()
        {
            var result = await _authService.Logout();

            if (!result)
            {
                await Application.Current.MainPage.DisplayAlert("Error", "Failed to logout, please try again.", "Ok");
            }

            await Shell.Current.GoToAsync($"//{nameof(LoginPage)}");
        }
    }
}
