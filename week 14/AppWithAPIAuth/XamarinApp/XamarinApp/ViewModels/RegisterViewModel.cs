﻿using Domain;
using MvvmHelpers.Commands;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using XamarinApp.Services;
using XamarinApp.Views;

namespace XamarinApp.ViewModels
{
    public class RegisterViewModel : ViewModelBase
    {
        private readonly IAuthenticationService _authService;

        public AsyncCommand RegisterButtonPressed { get; private set; }

        private RegisterRequest _registerRequest;
        public RegisterRequest RegisterRequest 
        {
            get => _registerRequest; 
            set
            {
                _registerRequest = value;
                OnPropertyChanged();
            } 
        }

        public RegisterViewModel()
        {
            _authService = DependencyService.Get<IAuthenticationService>();
            RegisterButtonPressed = new AsyncCommand(Register);
            RegisterRequest = new RegisterRequest();
        }

        private async Task Register()
        {
            if(RegisterRequest is null)
            {
                await Application.Current.MainPage.DisplayAlert("Error", "Please fill in the necessecary data.", "Ok");
                return;
            }

            if(string.IsNullOrEmpty(RegisterRequest.Email) || string.IsNullOrEmpty(RegisterRequest.Password) || string.IsNullOrEmpty(RegisterRequest.PasswordVerify))
            {
                await Application.Current.MainPage.DisplayAlert("Error", "Please fill in the necessecary data.", "Ok");
                return;
            }

            if(RegisterRequest.Password != RegisterRequest.PasswordVerify)
            {
                await Application.Current.MainPage.DisplayAlert("Error", "Passwords doesn't match up.", "Ok");
                return;
            }

            // Call auth service and register.
            var result = await _authService.Register(RegisterRequest);

            // Check if result is false, if it is display error.
            if(!result)
            {
                await Application.Current.MainPage.DisplayAlert("Error", "Error occured while registering. Try again.", "Ok");
                return;
            }

            // Yeet the user over to the login page.
            await Shell.Current.GoToAsync($"{nameof(LoginPage)}");
        }
    }
}
