﻿using MvvmHelpers.Commands;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using XamarinApp.Helpers;

namespace XamarinApp.ViewModels.SettingsViewModels
{
    public class ThemeSettingsViewModel : ViewModelBase
    {
        public bool RadioButtonSystemPreferences { get; set; }
        public bool RadioButtonLight { get; set; }
        public bool RadioButtonDark { get; set; }

        public AsyncCommand OnAppearing { get; set; }
        public AsyncCommand<CheckedChangedEventArgs> OnRadioButtonCheckedChanged { get; set; }

        // Fields.
        private bool _loaded;
        private bool _isFirstRun;

        public ThemeSettingsViewModel()
        {
            Title = "Theme Settings";

            OnAppearing = new AsyncCommand(OnAppearingMethod);
            OnRadioButtonCheckedChanged = new AsyncCommand<CheckedChangedEventArgs>(OnRadioButtonCheckedChangedMethod);

            switch (SettingsHelper.Theme)
            {
                case 0:
                    RadioButtonSystemPreferences = true;
                    break;
                case 1:
                    RadioButtonLight = true;
                    break;
                case 2:
                    RadioButtonDark = true;
                    break;
            }
        }

        private async Task OnAppearingMethod()
        {
            _loaded = true;
        }

        private async Task OnRadioButtonCheckedChangedMethod(object args)
        {
            if(!_loaded)
            {
                return;
            }    
            
            if(args is null)
            {
                return;
            }

            // If - for some reason - the radio button is not checked, return.
            if ( (args as CheckedChangedEventArgs).Value is false )
            {
                return;
            }

            
            if (RadioButtonSystemPreferences)
            {
                SettingsHelper.Theme = 0;
            }
            else if (RadioButtonLight)
            {
                SettingsHelper.Theme = 1;
            }
            else
            {
                SettingsHelper.Theme = 2;
            }

            // Change the theme.
            ThemeHandler.SetTheme();
        }
    }
}
