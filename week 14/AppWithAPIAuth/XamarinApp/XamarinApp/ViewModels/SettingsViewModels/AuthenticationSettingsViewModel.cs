﻿using MvvmHelpers.Commands;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Essentials;
using Xamarin.Forms;
using XamarinApp.Services;

namespace XamarinApp.ViewModels.SettingsViewModels
{
    public class AuthenticationSettingsViewModel : ViewModelBase
    {
        public string Token { get; set; }

        public AsyncCommand GetTokenCommand { get; private set; }

        public AuthenticationSettingsViewModel()
        {
            Title = "Authentication Settings";

            GetTokenCommand = new AsyncCommand(GetToken);
        }

        private async System.Threading.Tasks.Task GetToken()
        {
            Token = Preferences.Get("Token", "Failed to get token.");
            OnPropertyChanged(nameof(Token));
        }


    }
}
