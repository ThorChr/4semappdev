﻿using Domain;
using MvvmHelpers.Commands;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;
using XamarinApp.Services;
using XamarinApp.Views;

namespace XamarinApp.ViewModels
{
    public class LoginViewModel : ViewModelBase
    {
        private readonly IAuthenticationService _authService;

        public AsyncCommand LoginButtonPressed { get; private set; }
        public AsyncCommand RegisterButtonPressed { get; private set; }
        public AsyncCommand CheckIfTokenExists { get; private set; }

        public LoginRequest LoginRequest { get; set; }
        
        public LoginViewModel()
        {
            Title = "Login";

            // Dependency Injection.
            _authService = DependencyService.Get<IAuthenticationService>();

            LoginButtonPressed = new AsyncCommand(Login);
            RegisterButtonPressed = new AsyncCommand(NavigateToRegister);
            CheckIfTokenExists = new AsyncCommand(CheckIfTokenExistsAsync);

            LoginRequest = new LoginRequest();
        }

        private async Task Login()
        {
            if (LoginRequest is null)
            {
                return;
            }

            if (string.IsNullOrEmpty(LoginRequest.Email) || string.IsNullOrEmpty(LoginRequest.Password))
            {
                await Application.Current.MainPage.DisplayAlert("Login Error", "Please enter a username and password.", "OK");
                return;
            }

            try
            {
                var result = await _authService.Login(LoginRequest);

                if (!result)
                {
                    await Application.Current.MainPage.DisplayAlert("Login Error", "Invalid username or password.", "OK");
                    LoginRequest.Password = string.Empty;
                    OnPropertyChanged("LoginRequest");
                    return;
                }

                // The user is logged in, so clear the input fields, and navigate to the main page.
                LoginRequest = new LoginRequest();
                await Shell.Current.GoToAsync($"//{nameof(AboutPage)}");
            }
            catch (Exception ex)
            {
                await Application.Current.MainPage.DisplayAlert("Login Error", ex.Message, "OK");
            }
        }

        /// <summary>
        /// Used for checking if the user is logged in.
        /// So if a token exists in the preferences, we can just yeet the user to the proper application page.
        /// If not, they need to login.
        /// </summary>
        /// <returns></returns>
        private async Task CheckIfTokenExistsAsync()
        {
            var token = Preferences.Get("Token", "");
            if (token == "")
            {
                return;
            }

            await Shell.Current.GoToAsync($"//{nameof(AboutPage)}");
        }

        private async Task NavigateToRegister() => await Shell.Current.GoToAsync($"{nameof(RegisterPage)}");
    }
}
