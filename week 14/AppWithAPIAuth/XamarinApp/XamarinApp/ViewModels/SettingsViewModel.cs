﻿using MvvmHelpers;
using MvvmHelpers.Commands;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using XamarinApp.Models;
using XamarinApp.Views.SettingsPages;

namespace XamarinApp.ViewModels
{
    public class SettingsViewModel : ViewModelBase
    {
        public ObservableRangeCollection<SettingsItem> Pages { get; private set; }

        public AsyncCommand<SettingsItem> NavigateCommand { get; private set; }

        public SettingsViewModel()
        {
            Title = "Settings";

            Pages = new ObservableRangeCollection<SettingsItem>
            {
                new SettingsItem { Title = "Themes", NameOf = nameof(ThemeSettingsPage) },
                new SettingsItem { Title = "Authentication", NameOf = nameof(AuthenticationSettingsPage) },
            };

            NavigateCommand = new AsyncCommand<SettingsItem>(Navigate);
        }

        /// <summary>
        /// Navigates to the page specified.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        private async Task Navigate(SettingsItem item)
        {
            await Shell.Current.GoToAsync($"{item.NameOf}");
        }
    }
}
