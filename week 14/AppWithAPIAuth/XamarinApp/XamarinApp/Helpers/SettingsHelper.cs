﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Essentials;

namespace XamarinApp.Helpers
{
    /// <summary>
    /// Helper class for handling various settings and storing the value in the app's preferences.
    /// </summary>
    public static class SettingsHelper
    {
        // 0 = Default. 1 = Light. 2 = Dark.
        const int theme = 0;
        public static int Theme
        {
            get => Preferences.Get(nameof(Theme), theme);
            set => Preferences.Set(nameof(Theme), value);
        }
    }
}
