﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Essentials;

namespace XamarinApp.Helpers
{
    public class GenerateApiUri
    {
        public static Uri Get(string relativeUrl)
        {
            // Create URL.
            string baseAddress = DeviceInfo.Platform == DevicePlatform.Android ? "https://10.0.2.2:7149" : "https://localhost:7149";
            var uri = new Uri(String.Format(baseAddress + relativeUrl, string.Empty));

            return uri;
        }
    }
}
