﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace XamarinApp.Helpers
{
    /// <summary>
    /// Class used for settings the theme of the application.
    /// </summary>
    public static class ThemeHandler
    {
        public static void SetTheme()
        {
            switch(SettingsHelper.Theme)
            {
                // System Preferences.
                case 0:
                    App.Current.UserAppTheme = OSAppTheme.Unspecified;
                    break;

                // Light Theme.
                case 1:
                    App.Current.UserAppTheme = OSAppTheme.Light;
                    break;

                // Dark Theme.
                case 2:
                    App.Current.UserAppTheme = OSAppTheme.Dark;
                    break;
            }
        }
    }
}
