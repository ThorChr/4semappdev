﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace XamarinApp.Helpers
{
    public class CallApi
    {
        public static HttpClient _client { get; private set; } =  new HttpClient(new HttpClientHandler()
        {
            ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; }
        });

        public async static Task<HttpResponseMessage> PostAsync(Uri uri, StringContent stringContent)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, uri);
            request.Content = stringContent;

            var response = await _client.SendAsync(request);

            return response;
        }
    }
}
