﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XamarinApp.Models
{
    public class TestItem
    {
        public string Title { get; set; }
        public string Description { get; set; }
    }
}
