﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XamarinApp.Models
{
    public class SettingsItem
    {
        public string Title { get; set; }
        public string NameOf { get; set; }
        
    }
}
