﻿using Domain;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using XamarinApp.Helpers;

namespace XamarinApp.Services
{
    public class AuthenticationService : IAuthenticationService
    {
        /// <summary>
        /// Used for logging the user in based on the credentials provided via LoginRequest.
        /// </summary>
        /// <param name="loginRequest">Credentials provided from the user.</param>
        /// <returns>True or false.</returns>
        public async Task<bool> Login(LoginRequest loginRequest)
        {
            // Create URL.
            var uri = GenerateApiUri.Get("/api/Identity/Login");

            // Generate content.
            var content = new StringContent(JsonConvert.SerializeObject(loginRequest), Encoding.UTF8, "application/json");

            // Create request.
            var response = await CallApi.PostAsync(uri, content);

            if (!response.IsSuccessStatusCode)
            {
                return false;
            }

            // Deserialize result.
            var jsonFromResponse = await response.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<AuthenticationResult>(jsonFromResponse);

            // Store token to preferences.
            Preferences.Set("Token", result.Token);

            return true;
        }

        /// <summary>
        /// Logs the user out.
        /// </summary>
        /// <returns></returns>
        public async Task<bool> Logout()
        {
            try
            {
                // Remove token from local storage.
                Preferences.Remove("Token");

                return true;
            } catch {
                return false;
            }
        }

        /// <summary>
        /// Registers the user based on supplied credentials.
        /// </summary>
        /// <param name="registerRequest"></param>
        /// <returns></returns>
        public async Task<bool> Register(RegisterRequest registerRequest)
        {
            // Generate URL.
            var uri = GenerateApiUri.Get("/api/Identity/Register");

            // Generate content.
            var content = new StringContent(JsonConvert.SerializeObject(registerRequest), Encoding.UTF8, "application/json");

            // Create request.
            var response = await CallApi.PostAsync(uri, content);

            if (!response.IsSuccessStatusCode)
            {
                return false;
            }

            return true;
        }
    }
}
