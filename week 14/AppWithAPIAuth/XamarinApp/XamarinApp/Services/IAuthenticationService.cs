﻿using Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace XamarinApp.Services
{
    public interface IAuthenticationService
    {
        Task<bool> Login(LoginRequest loginRequest);
        Task<bool> Logout();
        Task<bool> Register(RegisterRequest registerRequest);
    }
}
