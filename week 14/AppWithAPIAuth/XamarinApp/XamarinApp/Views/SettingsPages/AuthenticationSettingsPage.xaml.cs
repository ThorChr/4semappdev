﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace XamarinApp.Views.SettingsPages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AuthenticationSettingsPage : ContentPage
    {
        public AuthenticationSettingsPage()
        {
            InitializeComponent();
        }
    }
}