# Project Idea

Meningen med det her projekt er at lave en app, der er i kontakt med en API der h�ndtere bruger authorisering.

S� n�r man starter appen op, bliver man m�dt med en login sk�rm (med en knap for at registere en bruger ved siden af).

N�r man er logget ind, s� er jeg lidt ligeglad med selve content, det er ikke hvad jeg fokuserer p�. <br/>
Men jeg vil have at der er en form for Settings menu, og i den menu skal der v�re mulighed for at:
- V�lge dark/light theme (eller sige om det skal f�lge systemet).
- M�ske v�lge nogle custom accent farver der bliver brugt i appen (kun et m�ske, fordi idk om det g�r ind og breaker dark/light theme... M�ske lave et "custom" theme, som bruger de her v�rdier?).

S� ye, jeg vil gerne fokusere p� at have en struktur for en sikker app.

# Project Overview

Det betyder der er nogle elementer projektet vil best� af:
- API projekt.
	- Registering.
	- Login.
	- Authorisering.
- Xamarin.Forms projekt.
	- Login.
	- Register.
	- Settings.

# Personal Goals

Jeg har selvf�lgeligt nogle m�l jeg g�r efter med det her projekt:
- Blive bedre til API authorisering.
- Blive bedre til Xamarin.Forms.
- Finde ud af hvordan man gemmer en JWT i en Xamarin.Forms app og g�r brug af den.
- Finde ud af hvordan man skifter dark/light/custom theme.
- Finde ud af hvordan man laver "components/classes" i Xamarin.Forms der g�r det muligt at style mange elementer, med �n ting.

De to f�rste er bare "blive bedre", men det er fordi jeg har allerede arbejdet med begge dele f�r, men der er altid ting man kan forbedre. S� jeg h�ber p� at ved jeg arbejder mere og mere med det, at jeg s� opfanger ekstra ting der g�r jeg l�rer noget nyt.

Og s� er der selvf�lgeligt de sidste par elementer, hvilket er konkrete m�k jeg vil g� efter.

# Misc Information

Som jeg troede, s� virker det til at v�re Xamarin.Forms delen der er den irriterende. Det som g�r det besv�rligt, er hvordan man kalder p� API'en, f�r skaffet brugerens JWT, og s� gemmer denne v�k s� den nemt kan bruges igen (og ogs� tjekker om brugeren er logget ind).
Det er den besv�rlige del lige pt...

Ser ud til man kan g�re brug af en Properties dictionary: https://stackoverflow.com/questions/46004200/where-to-store-tokens-in-a-xamarin-form-application <br/>
Hvis man g�r den rute, s� g�r jeg ud fra at man fra login gemmer token der, og s� bare lader brugeren bruge appen. S� alle steder hvor det kr�ver at brugeren udf�rer en handling mod API'en, s� tjekker man om den token eksisterer / er valid, if not, yeet til login side igen... Det virker bare s�... Ikke optimalt.

Anden mulighed kan v�re Xamarin.Auth: https://github.com/xamarin/docs-archive/tree/master/Recipes/xamarin-forms/General/store-credentials

Tredje mulighed er "Secure Storage" https://docs.microsoft.com/en-us/xamarin/essentials/secure-storage?tabs=android

Men de ting forklarer hvordan man gemmer token, det prim�re problem er hvordan man ved hver side, confirmer at brugeren er logget ind. Med mindre, man ikke beh�ver det?

---

Der er et par videoer jeg gerne vil have set, som forklarer / gennemg�r de forskellige funktionaliteter (eller andre ting, der bare er god code practice):
- [Dynamic App Themes in Xamarin.Forms - Light, Dark, & Custom Modes](https://www.youtube.com/watch?v=4w8TQ8njd3w&list=PLwOF5UVsZWUiHY1CkRVjYJ6dm0iCvAlfw&index=18)
- [Getting Started with Dependency Injection in Xamarin.Forms](https://www.youtube.com/watch?v=6CwQKqMpUFk&list=PLwOF5UVsZWUiHY1CkRVjYJ6dm0iCvAlfw&index=22)
- [Styling Xamarin.Forms Apps with Reusable Resources & Styles](https://www.youtube.com/watch?v=Se0yF5JXk70&list=PLwOF5UVsZWUiHY1CkRVjYJ6dm0iCvAlfw&index=17)

App themes og styling with reusable resources er noget der g�r h�nd i h�nd, og passer godt i projektet. Reusable styles er generelt ogs� bare god code practice. <br/>
Depedency Injection er noget jeg gerne vil l�re at g�re i Xamarin. Fordi m�den jeg h�ndtere min egen custom made service, er nok ikke p� den bedste m�de.

# Updates

## 07/04/2022 - Dag 2

S� ye, anden dag p� projektet her, og det g�r fremad.

I dag har jeg indtil videre fokuseret p� login sk�rmen, og s�rget for at det er muligt at logge ind. M�den jeg h�ndterede det hele p�, var at sige at Login sk�rmen var vores starter sk�rm, og s� ved at logge ind bliver man navigeret til selve appen. N�r dette sker, bliver ens navigation stack t�mt helt, s� man ikke kan navigere tilbage til login siden. <br/>
M�den jeg gemte JWT p�, var ved at g�re brug af Xamarin.Essentials.Preferences. S� n�r jeg logger en bruger ind, gemmer jeg token der inde, og n�r jeg logger dem ud, bliver den samme token slettet. For at tjekke om brugeren er logget ind, s� tjekker jeg om token er gemt der inde.

Om det er den bedste m�de, ved jeg ikke lige, men det virker fint for mit use case i hvert fald. Jeg ved man kan t�nke p� sikkerhed, men det er kun ens token der bliver gemt (g�r ud fra Preferences gemmer det lokalt i appens sandbox), hvilket realt set ikke er anderledes end n�r man gemmer en token i en persons Cookies. Det er en string der er gemt lokalt for den individuelle bruger. Derfor t�nker jeg det ikke er et problem, sikkerheds m�ssigt.

Registering af brugere virker ogs� nu via appen.

Fik ogs� �ndret alle entries jeg havde gjort brug af, s� de nu giver en bedre UX, s� n�r man skriver en email, er der et @ i ens tastatur osv. Derudover er password entries nu ogs� blokeret ud, s� man ikke kan se hele v�rdien som en string. <br />
Den [her side](https://docs.microsoft.com/en-us/xamarin/xamarin-forms/user-interface/text/entry) er en meget god ressource p� hvad Entry kan.

Som det sidste i l�bet af dagen, fik jeg arbejdet p� to bestemte ting:
- Dependency Injection. F�r gjorde jeg ikke brug af det, n�r jeg skulle g�re brug af en af mine services. S� det fik jeg fikset.
- Settings menu og page. Jeg brugte en del tid her, ogs� fordi jeg havde nogle problemer med routing stuff, men det betyder s� ogs� jeg har nu f�et en bedre forst�else for routing inden for Xamarin. <br/>
Nu har jeg s� en settings page, der best�r af en collection af elementer. N�r man trykker p� en af de elementer, kommer man ind p� en specifik side for pr�cist den setting, hvilket s� skal blive hooked op til sin egen VM, og p� den m�de kan man lave �ndringer.

