﻿using MauiBlazor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MauiBlazor.Services
{
    public interface IAnimeService
    {
        Task<IEnumerable<AnimeModel>> GetAnimes();
        Task<AnimeModel> GetAnime(int id);
    }
}
