﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MauiBlazor.Services
{
    public interface IAlertService
    {
        Task ShowAlert(string title, string message, string buttonText);
    }
}
