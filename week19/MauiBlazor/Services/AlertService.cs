﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MauiBlazor.Services
{
    public class AlertService : IAlertService
    {
        public async Task ShowAlert(string title, string message, string buttonText)
        {
            await App.Current.MainPage.DisplayAlert(title, message, buttonText);
        }
    }
}
