﻿using MauiBlazor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MauiBlazor.Services
{
    public class AnimeService : IAnimeService
    {
        /// <summary>
        /// Retrives a single anime based on ID.
        /// Returns null of nothing is found.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<AnimeModel> GetAnime(int id)
        {
            var animeEnumerable = await GetAnimes();
            var anime = animeEnumerable.FirstOrDefault(x => x.Id == id);

            return anime;
        }

        /// <summary>
        /// Used to get all animes.
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<AnimeModel>> GetAnimes()
        {
            var list = new List<AnimeModel>() 
            {
                new AnimeModel() { Id = 1, Title = "Darling in the FranXX", Rating = 10 },
                new AnimeModel() { Id = 2, Title = "Future Diary", Rating = 9 },
                new AnimeModel() { Id = 3, Title = "The Saga of Tanya the Evil", Rating = 10 },
            };

            return await Task.FromResult(list);
        }
    }
}
