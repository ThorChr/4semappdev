﻿using CommunityToolkit.Mvvm.Input;
using MauiApp1.Views;

namespace MauiApp1.ViewModels.Auth;

public partial class LoginViewModel
{
    [ICommand]
    public async Task LoginAsync()
    {
        await Shell.Current.GoToAsync($"//{nameof(HomePage)}", true);
        await Shell.Current.Navigation.PushAsync(new HomePage());
    }
}
