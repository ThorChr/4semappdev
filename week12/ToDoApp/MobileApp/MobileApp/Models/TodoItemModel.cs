﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace MobileApp.Models
{
    public class TodoItemModel
    {
        [PrimaryKey, AutoIncrement, Column("_id")]
        public int Id { get; set; }

        [NotNull]
        public string Title { get; set; }

        public string Image { get; set; }

        public bool Completed { get; set; }

        public DateTime StartDate { get; set; } = DateTime.Now;

        public DateTime? EndDate { get; set; }

        [Ignore]
        public string GetShortStartDate { get => StartDate.ToString("dd/MM/yyyy"); }
        
        [Ignore]
        public bool IsValid { get => (!string.IsNullOrEmpty(Title) && !string.IsNullOrWhiteSpace(Title)) ? true : false; }
    }
}
