﻿using MobileApp.Helpers;
using MobileApp.Models;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MobileApp.Data
{
    public class TodoItemDatabase
    {
        static SQLiteAsyncConnection Database;

        // Delays initialization of the database, until it's first accessed.
        public static readonly AsyncLazy<TodoItemDatabase> Instance = new AsyncLazy<TodoItemDatabase>(async () =>
        {
            var instance = new TodoItemDatabase();
            CreateTableResult result = await Database.CreateTableAsync<TodoItemModel>();
            return instance;
        });

        public TodoItemDatabase()
        {
            Database = new SQLiteAsyncConnection(Constants.DatabasePath, Constants.Flags);
        }

        /* Data Manipulation Methods */
        public Task<List<TodoItemModel>> GetItemsAsync()
        {
            return Database.Table<TodoItemModel>().ToListAsync();
        }

        public Task<List<TodoItemModel>> GetItemsNotDoneAsync()
        {
            return Database.QueryAsync<TodoItemModel>("SELECT * FROM [TodoItemModel] WHERE [Completed] = 0");
        }

        public Task<TodoItemModel> GetItemAsync(int id)
        {
            return Database.Table<TodoItemModel>().Where(x => x.Id == id).FirstAsync();
        }

        public Task<int> SaveItemAsync(TodoItemModel item)
        {
            if (item.Id != 0)
                return Database.UpdateAsync(item);
            else
                return Database.InsertAsync(item);
        }

        public Task<int> DeleteItemAsync(TodoItemModel item)
        {
            return Database.DeleteAsync(item);
        }

        // I can then further add more, e.g. getting all items that have an expiration set for some time this current week.
    }
}
