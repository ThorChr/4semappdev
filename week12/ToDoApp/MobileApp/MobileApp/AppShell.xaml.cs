﻿using MobileApp.Views;
using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace MobileApp
{
    public partial class AppShell : Xamarin.Forms.Shell
    {
        public AppShell()
        {
            InitializeComponent();
            Routing.RegisterRoute(nameof(TodoItemDetailsPage), typeof(TodoItemDetailsPage));
            Routing.RegisterRoute(nameof(TodoItemEditPage), typeof(TodoItemEditPage));
            Routing.RegisterRoute(nameof(TodoItemCreate), typeof(TodoItemCreate));
        }

        private async void OnMenuItemClicked(object sender, EventArgs e)
        {
            await Shell.Current.GoToAsync("//LoginPage");
        }
    }
}
