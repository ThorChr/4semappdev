﻿using MobileApp.Data;
using MobileApp.Models;
using MobileApp.Views;
using MvvmHelpers.Commands;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MobileApp.ViewModels
{
    [QueryProperty(nameof(ItemId), nameof(ItemId))]
    public class TodoItemDetailsViewModel : ViewModelBase
    {
        private int _itemId;
        public int ItemId
        {
            get { return _itemId; }
            set { _itemId = value; GetItemFromIdCommandAsync.ExecuteAsync(); }
        }

        private TodoItemModel _item;
        public TodoItemModel Item { 
            get => _item; 
            set 
            { 
                _item = value;
                OnPropertyChanged();
            } 
        }

        // Commands
        public AsyncCommand GetItemFromIdCommandAsync { get; set; }
        public AsyncCommand DeleteItemCommandAsync { get; set; }
        public AsyncCommand EditItemCommandAsync { get; set; }

        public TodoItemDetailsViewModel()
        {
            GetItemFromIdCommandAsync = new AsyncCommand(GetItemFromIdMethodAsync);
            DeleteItemCommandAsync = new AsyncCommand(DeleteItemMethodAsync);
            EditItemCommandAsync = new AsyncCommand(EditItemMethodAsync);
        }

        // Methods
        private async Task GetItemFromIdMethodAsync()
        {
            var db = await TodoItemDatabase.Instance;
            var item = await db.GetItemAsync(_itemId);

            if (item != null)
                Item = item;
            else
                await Shell.Current.GoToAsync($"///{nameof(TodoItemPage)}?{nameof(TodoItemViewModel.ForceRefresh)}={true}");
        }

        private async Task DeleteItemMethodAsync()
        {
            var db = await TodoItemDatabase.Instance;
            await db.DeleteItemAsync(Item);
            await Shell.Current.GoToAsync($"///{nameof(TodoItemPage)}?{nameof(TodoItemViewModel.ForceRefresh)}={true}");
        }

        private async Task EditItemMethodAsync()
        {
            await Shell.Current.GoToAsync($"{nameof(TodoItemEditPage)}?{nameof(TodoItemEditViewModel.ItemId)}={Item.Id}");
        }


    }
}
