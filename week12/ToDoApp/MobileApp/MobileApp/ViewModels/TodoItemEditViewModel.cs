﻿using MobileApp.Data;
using MobileApp.Models;
using MobileApp.Views;
using MvvmHelpers.Commands;
using Plugin.Media;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MobileApp.ViewModels
{
    [QueryProperty(nameof(ItemId), nameof(ItemId))]
    public class TodoItemEditViewModel : ViewModelBase
    {
        private int _itemId;
        public int ItemId
        {
            get { return _itemId; }
            set { 
                _itemId = value;
                GetItemFromIdCommandAsync.ExecuteAsync();
            }
        }

        private TodoItemModel _item;
        public TodoItemModel Item
        {
            get { return _item; }
            set { _item = value; OnPropertyChanged(); }
        }

        public AsyncCommand GetItemFromIdCommandAsync { get; set; }
        public AsyncCommand CapturePhotoAsyncCommand { get; set; }
        public AsyncCommand PickPhotoAsyncCommand { get; set; }
        public AsyncCommand SaveChangesAsyncCommand { get; set; }
        public AsyncCommand CancelChangesCommandAsync { get; set; }


        public TodoItemEditViewModel()
        {
            GetItemFromIdCommandAsync = new AsyncCommand(GetItemFromIdMethodAsync);
            CapturePhotoAsyncCommand = new AsyncCommand(CapturePhotoMethodAsync);
            PickPhotoAsyncCommand = new AsyncCommand(PickPhotoMethodAsync);
            SaveChangesAsyncCommand = new AsyncCommand(SaveChangesAsync);
            CancelChangesCommandAsync = new AsyncCommand(CancelChangesMethodAsync);
        }

        private async Task SaveChangesAsync()
        {
            if(Item.IsValid)
            {
                // Create db connection. Save changes to item, and clear out the current property of Item.
                var database = await TodoItemDatabase.Instance;
                await database.SaveItemAsync(Item);
                Item = new TodoItemModel();
                var list = await database.GetItemsAsync();

                await Shell.Current.GoToAsync($"///{nameof(TodoItemPage)}?{nameof(TodoItemViewModel.ForceRefresh)}={true}");
            }
        }

        private async Task GetItemFromIdMethodAsync()
        {
            var db = await TodoItemDatabase.Instance;
            var item = await db.GetItemAsync(_itemId);

            if (item != null)
                Item = item;
            else
                await Shell.Current.GoToAsync($"///{nameof(TodoItemPage)}?{nameof(TodoItemViewModel.ForceRefresh)}={true}");
        }

        private async Task CapturePhotoMethodAsync()
        {
            // Create a prompt for the user, to capture photo.
            var file = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions
            {
                Directory = "Test",
                SaveToAlbum = true,
                CompressionQuality = 75,
                CustomPhotoSize = 50,
                PhotoSize = Plugin.Media.Abstractions.PhotoSize.MaxWidthHeight,
                MaxWidthHeight = 2000,
                DefaultCamera = Plugin.Media.Abstractions.CameraDevice.Rear
            });

            if (file is null)
                return;

            // Store the location of the file.
            Item.Image = file.Path;
            OnPropertyChanged("Item");
        }

        private async Task PickPhotoMethodAsync()
        {
            var file = await CrossMedia.Current.PickPhotoAsync(new Plugin.Media.Abstractions.PickMediaOptions
            {
                PhotoSize = Plugin.Media.Abstractions.PhotoSize.Medium
            });

            if (file is null)
                return;

            Item.Image = file.Path;
            OnPropertyChanged("Item");
        }

        private async Task CancelChangesMethodAsync()
        {
            await Shell.Current.GoToAsync($"///{nameof(TodoItemPage)}");
        }
    }
}
