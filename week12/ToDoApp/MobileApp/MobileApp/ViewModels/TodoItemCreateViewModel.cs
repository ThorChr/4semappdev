﻿using MobileApp.Data;
using MobileApp.Models;
using MobileApp.Views;
using MvvmHelpers.Commands;
using Plugin.Media;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MobileApp.ViewModels
{
    public class TodoItemCreateViewModel : ViewModelBase
    {
        private TodoItemModel _item;
        public TodoItemModel Item 
        {
            get => _item; 
            set
            {
                _item = value;
                OnPropertyChanged();
            }
        }


        public AsyncCommand CreateNewItemCommandAsync { get; set; }
        public AsyncCommand CancelCreationCommandAsync { get; set; }
        public AsyncCommand CapturePhotoAsyncCommand { get; set; }
        public AsyncCommand PickPhotoAsyncCommand { get; set; }

        public TodoItemCreateViewModel()
        {
            Item = new TodoItemModel();

            CreateNewItemCommandAsync = new AsyncCommand(CreateNewItemMethodAsync);
            CancelCreationCommandAsync = new AsyncCommand(CancelCreationMethodAsync);
            CapturePhotoAsyncCommand = new AsyncCommand(CapturePhotoMethodAsync);
            PickPhotoAsyncCommand = new AsyncCommand(PickPhotoMethodAsync);
        }

        private async Task CreateNewItemMethodAsync()
        {
            // Check if the data is valid.
            if (Item.IsValid)
            {
                // Make connection to database.
                TodoItemDatabase database = await TodoItemDatabase.Instance;

                // Add stuff to database.
                await database.SaveItemAsync(Item);

                // Clean out the item property.
                Item = new TodoItemModel();

                var list = await database.GetItemsAsync();

                // Yeet back to overview, with a bool saying "yes we need to update the list".
                await Shell.Current.GoToAsync($"///{nameof(TodoItemPage)}?{nameof(TodoItemViewModel.ForceRefresh)}={true}");
            } else
            {
                // I suppose we can give an error somehow.
            }
        }

        private async Task CancelCreationMethodAsync()
        {
            await Shell.Current.GoToAsync($"///{nameof(TodoItemPage)}");
        }

        private async Task CapturePhotoMethodAsync()
        {
            // Create a prompt for the user, to capture photo.
            var file = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions
            {
                Directory = "Test",
                SaveToAlbum = true,
                CompressionQuality = 75,
                CustomPhotoSize = 50,
                PhotoSize = Plugin.Media.Abstractions.PhotoSize.MaxWidthHeight,
                MaxWidthHeight = 2000,
                DefaultCamera = Plugin.Media.Abstractions.CameraDevice.Rear
            });

            if (file is null)
                return;

            // Store the location of the file.
            Item.Image = file.Path;
        }

        private async Task PickPhotoMethodAsync()
        {
            var file = await CrossMedia.Current.PickPhotoAsync(new Plugin.Media.Abstractions.PickMediaOptions
            {
                PhotoSize = Plugin.Media.Abstractions.PhotoSize.Medium
            });

            if (file is null)
                return;

            Item.Image = file.Path;
        }
    }
}
