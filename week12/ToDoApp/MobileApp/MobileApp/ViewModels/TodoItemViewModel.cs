﻿using MobileApp.Data;
using MobileApp.Models;
using MobileApp.Views;
using MvvmHelpers;
using MvvmHelpers.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MobileApp.ViewModels
{
    [QueryProperty(nameof(ForceRefresh), nameof(ForceRefresh))]
    public class TodoItemViewModel : ViewModelBase
    {
        #region Properties and Fields
        public ObservableRangeCollection<TodoItemModel> TodoItems { get; set; }

        private bool _forceRefresh;
        public bool ForceRefresh
        {
            get => _forceRefresh;
            set 
            { 
                _forceRefresh = value;
                if(_forceRefresh)
                    RefreshCommandAsync.ExecuteAsync();
            }
        }

        private string _searchQueue;
        public string SearchQueue
        {
            get => _searchQueue;
            set 
            { 
                _searchQueue = value;
                // Makes sure we search on each change of queue.
                SearchCommandAsync.ExecuteAsync();
            }
        }

        private TodoItemModel _selectedItem;
        public TodoItemModel SelectedItem
        {
            get => _selectedItem;
            set 
            {
                
                if(value != null)
                {
                    _selectedItem = value;
                    SelectedItem = null;
                }

                OnPropertyChanged();
            }
        }


        #endregion

        #region Commands
        public AsyncCommand SearchCommandAsync { get; set; }
        public AsyncCommand RefreshCommandAsync { get; }
        public AsyncCommand<TodoItemModel> NavigateCommandAsync { get; set; }
        public AsyncCommand CreateNewItemCommandAsync { get; set; }
        public AsyncCommand<TodoItemModel> NavigateToEditCommandAsync { get; set; }
        public AsyncCommand<TodoItemModel> ChangeCompletedStatusCommandAsync { get; set; }
        public AsyncCommand<TodoItemModel> DeleteItemCommandAsync { get; set; }
        
        #endregion

        public TodoItemViewModel()
        {
            SearchCommandAsync = new AsyncCommand(SearchMethodAsync);
            RefreshCommandAsync = new AsyncCommand(RefreshMethodAsync);
            NavigateCommandAsync = new AsyncCommand<TodoItemModel>(NavigateMethodAsync);
            CreateNewItemCommandAsync = new AsyncCommand(CreateNewItemMethodAsync);
            NavigateToEditCommandAsync = new AsyncCommand<TodoItemModel>(NavigateToEditMethodAsync);
            ChangeCompletedStatusCommandAsync = new AsyncCommand<TodoItemModel>(ChangeCompletedStatusMethodAsync);
            DeleteItemCommandAsync = new AsyncCommand<TodoItemModel>(DeleteItemMethodAsync);

            // This will be done synchronously.
            GetData();
        }

        #region Methods
        /// <summary>
        /// Used for searching through the observable collection.
        /// </summary>
        /// <returns></returns>
        private async Task SearchMethodAsync()
        {
            await RefreshMethodAsync();

            // Guard Clause for checking if we actually need to search.
            if (_searchQueue == "" || _searchQueue == null || _searchQueue == " ")
                return;

            // Find what the user is searching for.
            var localSearchQueue = _searchQueue.Trim().ToLower();
            var newCollection = TodoItems.Where(x => x.Title.ToLower().Contains(localSearchQueue)).ToList();

            // Update the collection.
            TodoItems.Clear();
            TodoItems.AddRange(newCollection);
            OnPropertyChanged("TodoItems");
        }

        /// <summary>
        /// Makes a call to GetData, thus refreshing data.
        /// </summary>
        /// <returns></returns>
        private async Task RefreshMethodAsync()
        {
            IsBusy = true;

            await GetData();

            IsBusy = false;
        }

        /// <summary>
        /// Navigates user to new page.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        private async Task NavigateMethodAsync(TodoItemModel item)
        {
            await Shell.Current.GoToAsync($"{nameof(TodoItemDetailsPage)}?{nameof(TodoItemDetailsViewModel.ItemId)}={item.Id}");
        }

        /// <summary>
        /// Navigates user to "create new item" page.
        /// </summary>
        /// <returns></returns>
        private async Task CreateNewItemMethodAsync()
        {
            await Shell.Current.GoToAsync($"{nameof(TodoItemCreate)}");
        }

        /// <summary>
        /// Retrieves data and updates the ObervableRangeCollection.
        /// </summary>
        /// <returns></returns>
        public async Task GetData()
        {
            IsBusy = true;

            TodoItems = new ObservableRangeCollection<TodoItemModel>();

            // Get database connection.
            try
            {
                TodoItemDatabase database = await TodoItemDatabase.Instance; // For some reason, this line doesn't work anymore.
                List<TodoItemModel> itemList = await database.GetItemsAsync();

                if (itemList != null && itemList.Count > 0)
                {
                    TodoItems.Clear();
                    TodoItems.AddRange(itemList);
                    OnPropertyChanged("TodoItems");
                }
            } catch (Exception ex)
            {
                var message = ex.Message;
            }

            IsBusy = false;
        }

        private async Task NavigateToEditMethodAsync(TodoItemModel item)
        {
            await Shell.Current.GoToAsync($"{nameof(TodoItemEditPage)}?{nameof(TodoItemEditViewModel.ItemId)}={item.Id}");
        }

        private async Task ChangeCompletedStatusMethodAsync(TodoItemModel item)
        {
            IsBusy = true;

            item.Completed = !item.Completed;

            var db = await TodoItemDatabase.Instance;
            await db.SaveItemAsync(item);
            var list = await db.GetItemsAsync();
            TodoItems.Clear();
            TodoItems.AddRange(list);

            IsBusy = false;
        }

        private async Task DeleteItemMethodAsync(TodoItemModel item)
        {
            IsBusy = true;

            var db = await TodoItemDatabase.Instance;
            await db.DeleteItemAsync(item);
            await GetData();

            IsBusy = false;
        }
        #endregion
    }
}
