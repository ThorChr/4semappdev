# ToDo mobile application using Xamarin.Forms and SQLite

Goal with this project is to have a to-do application up and running. Data has persistency by utilizing SQLite. App should hopefully be crossplatform by utilizing Xamarin.Forms. However, I can only test the Android version.

# Remarks / Useful links

I am going to be leaning on Microsoft's own documentation quite heavily. Furthermore I will also be using the nuget package "MVVMHelpers" from James Montemagno.

Also, I am going to be writing quite a lot in the process section, but it's primarily going to be for myself, as a form of note keeping.

- [Xamarin.Forms Local Databases](https://docs.microsoft.com/en-us/xamarin/xamarin-forms/data-cloud/data/databases)

# Process

I am going to be dividing this section into a few days, corresponding to the days that I am working on this project.

## 2022/03/23 - Wednesday

First of, I created a new project of the type "Xamarin.Forms" with a flyout menu (burger menu). I went ahead and deleted all base files, meaning I only had a simply "AboutPage" file left. I then installed MVVMHelpers, and setup a "ViewModelBase" class, that all further ViewModels can inherit from.

Then I installed the [*sqlite-net-pcl*](https://www.nuget.org/packages/sqlite-net-pcl/) nuget package. After that I created a new folder called Helpers, and created a constants class, which contains some information needed for the SQLite database.

Next step is a **database access class**, which is used for *".. centralizing query logic and simplifying the management of database initialization, making it easier to refactor or expand data operations as the app grows."* <br />
We are making sure our database connection variable is a static field, which ensures that we only use a single database connection for the life of the app. A persistent static connection offers better performance, than opening and closing connections multiple times during an app session.<br />
This access class make suse of **lazy loading**, which is something that can be read up on here: https://devblogs.microsoft.com/pfxteam/asynclazyt/

I then went ahead and created a basic model for each Todo item. 

After that, I added some methods inside the TodoItemDatabase.cs class. These methods are for data manipulation. Think of these as the usual EF methods for getting and adding data. <br />
It's possible to use custom SQL queries from inside these methods.

After adding the methods for data manipulation, it seems like the next step is actually getting the app ready regarding it's UI. When the UI section is "finished", I'll be able to make use of the database like so:

```C#
async void OnSaveClicked(object sender, EventArgs e)
{
    var todoItem = (TodoItem)BindingContext;
    TodoItemDatabase database = await TodoItemDatabase.Instance;
    await database.SaveItemAsync(todoItem);

    // Navigate backwards
    await Navigation.PopAsync();
}
```

But for now, as I said, time for some UI stuff. I am going to make it be relatively simple.

I seem to remember something about a "modal" page, which I believe is like, a new page is being overlayed upon the current page. iOS handles it really well, so I want to use that for the creation of a new item.

I learned that you can add a button in the title view / title bar of a page by doing this:
```xaml
<ContentPage.ToolbarItems>
    <ToolbarItem Text="Add" />
</ContentPage.ToolbarItems>
```

Right, now I've created a page and the corresponding ViewModel, for adding a new item. Now it's time to establish a DB connection and add the item.

Cool, now I can add item to my list. I do have one problem however. I don't know where to inspect the data. When I'm using a normal SQL database, I can make use of the *SQL Server Object Explorer* but with SQLite, I have no idea. That is something to read up on at a later point.

Now I need to make sure the overview page actually pulls data from the Database.

Sooo yeah, that is actually more difficult that I thought it would be. Visual Studio kinda freezes up on me, when trying to code this stuff. Or rather, when I'm trying to debug it - massive freeze. Kinda annoying, need to find fix.

Okay okay okay, I fixed the other thing, now I found another very weird issue. I have trouble saving / deleting stuff from the database. I am unsure of if items gets a PK, and I am unsure of why my database sometimes just straight up resets. <br />
I checked, and for some reason, the database does not auto increment the ID of the item. So when I'm deleting something by the ID, it's deleting EVERYTHING... I think, I think that's what's going on.

Okay! I made it kinda work I think. As far as I know now, the database actually Auto Increments the PK now. Which means I can add items, and delete a specific item, without affecting the others. 

### Summary for today.

- Created the base project.
- Added SQLite, and a Data Access Layer to interact with the database.
- Added overview for the todo items.
- Added detailed information for an individual item.
- Added option to both create and delete items.
- Heavy, heavy focus on MVVM. No use of code-behind.

### Stuff for next time.

- Page for editing the items, with two ways of accessing this.
	* On the detailed page, add another button "Edit".
	* Swipe Gesture, so the user can swipe on an item.
- Swipe gesture for deleting an item.
- Image functionality, so the user can add a picture.
	* Start off with just a text box that takes a link.
	* Then find out how to access camera.
	* Then let the user take a picture, and use the on device location of that picture, instead of the link.
- If I have enough time, then a *frontpage* with some stats (basic stats, how many complete vs not complete).

That should be it. After those features, the project should be complete.

## 2022/03/30 - Wednesday

Right, started out real annoying. For some reason, the program simply didn't work despite no changes had been made since last time. The fix, ended up being a reinstall of the SQLite Nuget package. Why? No idea. It just worked though.

Seems like I wasn't the only one with that problem: https://stackoverflow.com/questions/46915404/the-type-initializer-for-sqlite-sqliteconnection-threw-an-exception

Anyways, as I mentioned last time, what I wanted to do now was primarily 2 things:
* Editing functionality.
* Adding a photo to an item.

Let's try and get started with the photo feature.

Fast forward an hour or so, and that was a pain to set up. For some reason, even when I followed the setup guide and copy pasted their information, the project gave me errors. Then I tried doing exactly what I did in a previous project, and it still failed. I ended up getting it working after deleting all files / code changes I had made, and copy paste the code from the guide once again.

Well, at least it works now. So now I can actually get to coding, 2 hours after starting work on this project.

Firstly, let's try to make it so the user can add a picture when creating a todo item.

Right, seems like adding pictures to the item is now possible. That took roughly 20 minutes, maybe a bit less. <br/>
Next step would be the ability to edit an already existing item.

Right, editing is now possible as well. And with that we now have CRUD functionality for the todo item list.

And with that I am done with this project. It was interesting and I learned something new.

I ended up switching from ListView to CollectionView, which meant I had to rework a few things, but it was all good. Now I have full swipe support, which I really like.

If there is one thing I want to improve on, it's the massive code repetition. I think I could fix this by using an abstract class, or just a parent class. But a lot of the methods are the same from Create and Edit, and the same goes for Canceling an operation. So this is one point for improvement.

Oh also, the handling for the swipe commands, I think to make it be "proper" I should have made it so the command "loaded" up the relevant page, and included a GET variable, which told the ViewModel what it needed to do. Because right now, that specific CRUD functionality is just added in the Overview VM for the items - which I don't think is proper.

But even with that, I'll still say that this right here was a solid project, and I completed the goals I had set up for myself. 

- MVVM.
- CRUD functionality.
- Image upload.
- SQLite persistence.