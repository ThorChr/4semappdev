﻿using MvvmHelpers.Commands;
using Plugin.Media;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CameraApp.ViewModels
{
    public class CameraViewModel : ViewModelBase
    {
        public ImageSource ImgSource { get; set; }

        private string photoPath;
        public string PhotoPath
        {
            get { return photoPath; }
            set { photoPath = value; OnPropertyChanged(); }
        }

        // Commands
        public AsyncCommand CapturePhotoAsyncCommand { get; }
        public AsyncCommand PickPhotoAsyncCommand { get; set; }

        public CameraViewModel()
        {
            CapturePhotoAsyncCommand = new AsyncCommand(CapturePhotoAsync);
            PickPhotoAsyncCommand = new AsyncCommand(PickPhotoAsync);
        }

        // Methods
        async Task CapturePhotoAsync()
        {
            // Check to see if we can use the feature at all.
            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakeVideoSupported)
            {
                await Application.Current.MainPage.DisplayAlert("Not supported", "This feature is not supported on your device.", "OK");
                Console.WriteLine("Not supported.");
                return;
            }

            // Prompts the user with the camera so they can take a picture.
            var file = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions 
            {
                Directory = "Test",
                SaveToAlbum = true,
                CompressionQuality = 75,
                CustomPhotoSize = 50,
                PhotoSize = Plugin.Media.Abstractions.PhotoSize.MaxWidthHeight,
                MaxWidthHeight = 2000,
                DefaultCamera = Plugin.Media.Abstractions.CameraDevice.Rear
            });

            if (file == null)
                return;

            await Application.Current.MainPage.DisplayAlert("File location", file.Path, "OK");
            Console.WriteLine($"File location: {file.Path}");

            // Get the image source, and save it to a property that is databound to a XAML element in the view.
            ImgSource = ImageSource.FromStream(() =>
            {
                var stream = file.GetStream();
                file.Dispose();
                return stream;
            });
            OnPropertyChanged(nameof(ImgSource));
        }

        async Task PickPhotoAsync()
        {
            if(!CrossMedia.Current.IsPickPhotoSupported)
            {
                await Application.Current.MainPage.DisplayAlert("Not supported", "This feature is not supported on your device.", "OK");
                Console.WriteLine("Not supported.");
                return;
            }

            var file = await Plugin.Media.CrossMedia.Current.PickPhotoAsync(new Plugin.Media.Abstractions.PickMediaOptions 
            {
                PhotoSize = Plugin.Media.Abstractions.PhotoSize.Medium
            });

            if (file == null)
                return;

            ImgSource = ImageSource.FromStream(() => 
            {
                var stream = file.GetStream();
                file.Dispose();
                return stream;
            });

            OnPropertyChanged(nameof(ImgSource));
        }
    }
}
