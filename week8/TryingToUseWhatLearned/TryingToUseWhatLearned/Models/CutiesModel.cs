﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TryingToUseWhatLearned.Models
{
    public class CutiesModel
    {
        public string Name { get; set; }
        public string Show { get; set; }
        public string Image { get; set; }
    }
}
