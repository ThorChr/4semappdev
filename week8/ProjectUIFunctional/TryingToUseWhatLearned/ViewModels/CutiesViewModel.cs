﻿using MvvmHelpers;
using MvvmHelpers.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TryingToUseWhatLearned.Models;
using Xamarin.Forms;

namespace TryingToUseWhatLearned.ViewModels
{
    public class CutiesViewModel : ViewModelBase
    {
        /*
         * Inside our ViewModel we want to have a lot of things.
         * We are trying to make use of databinding as much as we possibly can, this does however also mean a lot of things will be added in here.
         * As far as I can see, there is basically a 1:1 correlation between a View and a ViewModel. So yeah, each view has its own viewmodel.
         * Anyhow, the CutiesPage is supposed to show a list of all the cuties we have added. There is no detailed description of it. So only name, show and image.
         * Note: We will have some things that don't really do anything, because storing such things would require database connection and whatnot. But that's not important.
         * The purpose of this project is to make a "proper" project, that I can always reference in the future.
         */

        // Properties.
        /* These properties are used for MVVM. */
        private CutiesModel previouslySelectedCutie;
        private CutiesModel selectedCutie;
        public CutiesModel SelectedCutie
        {
            get => selectedCutie;
            set 
            { 
                if(value != null)
                {
                    previouslySelectedCutie = value;
                    Application.Current.MainPage.DisplayAlert("Selected", value.Name, "OK");
                    value = null;
                }

                selectedCutie = value;
                OnPropertyChanged();
            }
        }

        private string searchQueue;

        public string SearchQueue
        {
            get => searchQueue;
            set 
            { 
                searchQueue = value;
                OnPropertyChanged();
            }
        }




        // Collections. 
        /* These hold all the items we wish to display to the user. */
        public ObservableRangeCollection<CutiesModel> Cuties { get; set; }

        // Commands.
        /* These will be fired when we want to react to something the user does. */
        public AsyncCommand RefreshCommand { get; }
        public AsyncCommand<CutiesModel> FavoriteCommand { get; set; }
        public AsyncCommand SearchCommand { get; set; }

        // Constructor.
        /* We use it as we would use a constructor in any other C# project. We want to initialize some data and commands, so we do it inside here. */
        public CutiesViewModel()
        {
            Title = "Cuties showcase";

            // Add dummy data.
            AddDummyData();

            // Initialize commands.
            RefreshCommand = new AsyncCommand(Refresh);
            FavoriteCommand = new AsyncCommand<CutiesModel>(Favorite);
            SearchCommand = new AsyncCommand(Search);
        }

        // Methods that the commands will call and execute.
        /* As mentioned above, all the methods are something that will be called by the commands. And the commands are being called upon from the view. So this way we make sure to make use of MVVM. */
        async Task Refresh()
        {
            IsBusy = true;

            await Task.Delay(1000);

            IsBusy = false;
        }

        async Task Favorite(CutiesModel cutie)
        {
            if (cutie == null)
                return;

            await Application.Current.MainPage.DisplayAlert("Favorite", cutie.Name, "OK");
        }

        async Task Search()
        {
            await AddDummyData();

            if (searchQueue == string.Empty || searchQueue == null)
                return;

            var localSearchQueue = searchQueue.Trim().ToLower();
            var newCollection = Cuties.Where(x => x.Name.ToLower().Contains(localSearchQueue)).ToList();

            // This should replace the search results.
            Cuties.Clear();
            Cuties.AddRange(newCollection);
            OnPropertyChanged(nameof(Cuties));
        }

        private async Task AddDummyData()
        {
            // Initialize collections.
            Cuties = new ObservableRangeCollection<CutiesModel>();

            // Add dummy data to collections.
            Cuties.Add(new CutiesModel() { Name = "Miko Yotsuya", Show = "Mieruko-chan", Image = "https://i.imgur.com/dpSANZP.jpg" });
            Cuties.Add(new CutiesModel() { Name = "Marin Kitagawa", Show = "My Dress-Up Darling", Image = "https://i.imgur.com/QPjzPdK.jpg" });
            Cuties.Add(new CutiesModel() { Name = "Vladilena Milizé", Show = "86 -Eighty Six-", Image = "https://i.imgur.com/rZ5qwp9.jpg" });
            Cuties.Add(new CutiesModel() { Name = "Isuzu Sento", Show = "Amagi Brilliant Park", Image = "https://i.imgur.com/24rbLdj.png" });
            Cuties.Add(new CutiesModel() { Name = "Nana Hiiragi", Show = "Talentless Nana", Image = "https://i.imgur.com/SmdXrhb.jpg" });
            Cuties.Add(new CutiesModel() { Name = "Rem", Show = "Re:Zero", Image = "https://i.imgur.com/V2qwVt5.png" });
            Cuties.Add(new CutiesModel() { Name = "Mariya Shidō", Show = "Maria✝Holic", Image = "https://i.imgur.com/36ltuMY.jpg" });
        
            OnPropertyChanged(nameof(Cuties));
        }
    }
}
